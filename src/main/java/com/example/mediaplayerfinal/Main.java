package com.example.mediaplayerfinal;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * @author Denny, Nils, Mads
 * Main class. Used for starting the program and opening new windows.
 */
public class Main extends Application {


    /**
     * Main method for starting. Opens first window.
     * @param stage
     * @throws IOException
     */
    @Override
    public void start(Stage stage) throws IOException {
        Main.playlistEditWindow();
    }

    /**
     * Opens Playlist Editor window.
     * @throws IOException
     */
    public static void playlistEditWindow() throws IOException {
        Stage playlistEditorWindow = new Stage();

        FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("PlaylistEdit.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 500, 500);
        playlistEditorWindow.setResizable(false);
        playlistEditorWindow.setTitle("Choose a File or Playlist");
        playlistEditorWindow.setScene(scene);
        playlistEditorWindow.show();
    }

    /**
     * Opens Player window.
     * @throws IOException
     */
    public static void playerWindow() throws IOException {
        Stage playerWindow = new Stage();

        FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("Player.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 1200, 800);
        playerWindow.setTitle("Media Player");
        playerWindow.setScene(scene);
        playerWindow.show();
    }

    /**
     * Opens Dialogue window for creating and deleting playlists.
     * @throws IOException
     */
    public static void createDeletePlaylist() throws IOException {
        Stage createPlaylistWindow = new Stage();

        FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("CreateDeletePlaylist.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 600, 300);
        createPlaylistWindow.setResizable(false);
        createPlaylistWindow.setTitle("Create/delete Playlist");
        createPlaylistWindow.setScene(scene);
        createPlaylistWindow.showAndWait();
    }


    /**
     * Starting point for program.
     * @param args
     */
    public static void main(String[] args) {
        launch();
    }
}