
package com.example.mediaplayerfinal;


import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.media.*;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * @author Denny, Nils, Mads
 * Controller class for the media player.
 */
public class PlayerController implements Initializable{


    @FXML
    private MediaView mediaView;

    @FXML
    private Button skip, PlayPause, filesAndPlaylists;

    @FXML
    private BorderPane borderPane;


    private MediaPlayer mp;
    private Media me;



    public int vid = -1;
    public static ArrayList<File>playlist= new ArrayList();

    /**
     * makes mediaplayer and reads the next file from the currently-playing arraylist
     */
    public void makeMediaPlayer(){

        if(mp !=null){
           mp.stop();
        }
        if(vid>=playlist.size()-1){
            vid=-1;
        }

        me = new Media(getNext().toURI().toString());

        mp = new MediaPlayer(me);

        mediaView.setMediaPlayer(mp);

        mp.setAutoPlay(false);

        mp.setOnEndOfMedia(new Runnable() {
            @Override
            public void run() {
                makeMediaPlayer();
            }
        });
    }

    public File getNext(){
        this.vid++;
        return playlist.get(vid);
    }


    /**
     * initializes the method
     * @param location
     * @param resources
     */
    public void initialize(URL location, ResourceBundle resources){
        makeMediaPlayer();
    }


    int i = 1;
    /**
     * method for play/pause button
     */
    @FXML
    private void handlePlay()
    {
        if(i==1) {
            mp.play();
            i=2;
        }else {
            mp.pause();
            i=1;
        }

    }

    /**
     * skip track
     */
    @FXML
    private void setSkip(){
        makeMediaPlayer();
    }

    /**
     * stops mediaplayer
     */
    @FXML
    private void stop(){
        mp.stop();
    }

    /**
     * button for going back to playlist editor
     * @throws IOException
     */
    @FXML
    private void filesAndPlaylistsBtn() throws IOException {
        mp.stop();
        Main.playlistEditWindow();
        Stage s = (Stage)filesAndPlaylists.getScene().getWindow();
        s.close();
    }
}