package com.example.mediaplayerfinal;

import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.util.Objects;

/**
 * @author Denny, Nils, Mads
 * dialogue window for creating and deleting playists
 */
public class CreateDeletePlaylistController {


    @FXML
    private TextField textCreate, txtDescription;
    @FXML
    private ComboBox<String> dropdownDelete;


    /**
     * initializes the window.
     * gets all needed information from the database.
     */
    @FXML
    protected void initialize(){
        //clear for refresh
        dropdownDelete.getItems().clear();
        textCreate.clear();

        //get playlists from database and show them in dropdown
        DB.selectSQL("select fldTitle from tblPlaylist");
        do{
            String data = DB.getDisplayData();
            if (data.equals(DB.NOMOREDATA)){
                break;
            }else{
                dropdownDelete.getItems().add(data);
            }
        } while(true);

    }

    /**
     * creates a new playlist with title and description from the textFields.
     */
    @FXML
    protected void btnCreate(){

        String title = textCreate.getText();
        String description = txtDescription.getText();
        boolean valid = true;


        DB.selectSQL("select fldTitle from tblPlaylist");
        do{
            String data = DB.getDisplayData();
            if (data.equals(DB.NOMOREDATA)){
                break;
            }else{
                data = data.replace(" ", "");
                data = data.replace("\n", "");
                if(data.equals(title)){
                    valid = false;
                }
            }
        } while(true);


        if(valid){
            if(!Objects.equals(title, "") && !title.contains(" ")) {
                DB.insertSQL("insert into tblPlaylist(fldTitle, fldDescription) values('" + title + "', '" + description + "')");
                Alert alert = new Alert(Alert.AlertType.INFORMATION, "Playlist created", ButtonType.OK);
                alert.showAndWait();
            }else {
                Alert alert = new Alert(Alert.AlertType.ERROR, "Title cannot contain spaces or be empty.", ButtonType.OK);
                alert.showAndWait();
            }
        }else{
            Alert alert = new Alert(Alert.AlertType.ERROR, "Playlist is already existing.", ButtonType.OK);
            alert.showAndWait();
        }
        initialize();

    }

    /**
     * gets playlist selection from the combobox next to it and deletes that playlist.
     */
    @FXML
    protected void btnDelete(){
        //get selection
        String selection = dropdownDelete.getSelectionModel().getSelectedItem();

        if(selection != null) {
            selection = selection.replace(" ", "");
            selection = selection.replace("\n", "");

            //delete selection from database
            DB.deleteSQL("delete from [tblFile-Playlist] where fldPlaylistID = (select fldID from tblPlaylist where fldTitle = '" + selection + "')");
            DB.deleteSQL("delete from tblPlaylist where fldTitle = '" + selection + "'");

            //show succes message
            Alert alert = new Alert(Alert.AlertType.INFORMATION, "Playlist deleted", ButtonType.OK);
            alert.showAndWait();

            //refresh window
            initialize();
        }else{
            Alert alert = new Alert(Alert.AlertType.ERROR, "Please select a playlist first.", ButtonType.OK);
            alert.showAndWait();
        }
    }

}
