package com.example.mediaplayerfinal;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;

/**
 * @author Denny,Nils,Mads
 * Controller class for main window. Houses all methods for Playlist editor.
 */
public class PlaylistEditController {

    @FXML
    private ListView<String> list;
    @FXML
    private ListView<String> list2;
    @FXML
    private ComboBox<String> Dropdown;
    @FXML
    public Button playFile, playPlaylist;
    @FXML
    private TextField txtSearch;
    @FXML
    private Label labelFile1, labelFile2, labelPlaylist;

    private int selectedPlaylistID;
    private final int mediaFolder = 1;        //for later extension to use multiple media folders


    /**
     * Initializes the window.
     * Read values from database and display them in Dropdown menu and lists.
     * Clear methods used for refreshing page
     */
    @FXML
    protected void initialize() {

        //get playlists show in dropdown menu
        DB.selectSQL("select fldTitle from tblPlaylist");
        do{
            String data = DB.getDisplayData();
            if (data.equals(DB.NOMOREDATA)){
                break;
            }else{
                Dropdown.getItems().add(data);
            }
        } while(true);


        //get filenames and show in list1
        DB.selectSQL("select fldFileName from tblFile");
        do{
            String data = DB.getDisplayData();
            if (data.equals(DB.NOMOREDATA)){
                break;
            }else{
                list.getItems().add(data);
            }
        } while(true);


        //set default value for dropdown menu
        Dropdown.getSelectionModel().selectFirst();
        String selectedPlaylist = Dropdown.getSelectionModel().getSelectedItem();

        getIDforPlaylist(selectedPlaylist);

        DB.getData();   //fixes sql error in next query (probably because there is pending data)

        setList2();
        setLabelFile();
        setLabelPlaylist();
    }


    /**
     * input from combobox (dropdown).
     * chooses a playlist from the database based on combobox selection.
     */
    @FXML
    protected void choosePlaylist(){

        //get item that is selected in dropdown and convert to string
        String selectedPlaylist = Dropdown.getSelectionModel().getSelectedItem();
        getIDforPlaylist(selectedPlaylist);

        //clear list
        list2.getItems().clear();

        //get entries for selected playlist and display them
        setList2();

        setLabelPlaylist();
    }

    /**
     * sets file attributes to label when list is clicked.
     */
    @FXML
    protected void list1clicked(){
        setLabelFile();
    }


    /**
     * Button for searching files.
     * searches database for filename, artist and title.
     */
    @FXML
    protected void btnSearch(){
        list.getItems().clear();
        String search = txtSearch.getText();

        DB.selectSQL("select fldFileName from tblFile where " +
                "fldArtist like '%" + search + "%' or fldFileName like '%" + search + "%' or fldTitle like '%" + search + "%'");
        do{
            String data = DB.getDisplayData();
            if (data.equals(DB.NOMOREDATA)){
                break;
            }else{
                list.getItems().add(data);
            }
        } while(true);
    }


    /**
     * refresh page.
     * clears all lists and menus and calls initialize().
     */
    @FXML
    protected void refresh(){
        list.getItems().clear();
        list2.getItems().clear();

        txtSearch.clear();

        Dropdown.getItems().clear();

        initialize();
    }


    /**
     * opens a dialogue box for creating and deleting playlists and waits for a user input.
     * @see CreateDeletePlaylistController
     * @throws IOException
     */
    @FXML
    protected void createDeletePlaylist() throws IOException {
        Main.createDeletePlaylist();
        refresh();
    }





    /**
     * Button with an arrow to the right.
     * Add the selected item from files to the selected playlist.
     */
    @FXML
    protected void ButtonClickR() {
        if (list.getSelectionModel().getSelectedItem() != null) {

            //get what is selected
            String move = list.getSelectionModel().getSelectedItem();
            list2.getItems().add(move);

            //add object to playlist
            move = move.replace("\n", "");
            DB.insertSQL("insert into [tblFile-Playlist] values ('" + selectedPlaylistID + "', (select fldFileName from tblFile where fldFileName = '" + move + "'))");

        } else {
            //error message for not selecting a file
            Alert alert = new Alert(Alert.AlertType.ERROR, "Please select a file first", ButtonType.OK);
            alert.showAndWait();
        }
    }

    /**
     * Button with an arrow to the left.
     * Remove the selected item from a playlist.
     */
    @FXML
    protected void ButtonClickL() {

        if(list2.getSelectionModel().getSelectedItem() != null) {
            String remove = list2.getSelectionModel().getSelectedItem();
            list2.getItems().remove(remove);

            remove = remove.replace("\n", "");
            DB.deleteSQL("delete top(1) from [tblFile-Playlist] where fldFileName = '" + remove + "' and fldPlaylistID = '" + selectedPlaylistID + "'");
        } else {
            //error message for not selecting a file
            Alert alert = new Alert(Alert.AlertType.ERROR, "Please select a file first", ButtonType.OK);
            alert.showAndWait();
        }
    }


    /**
     * Method gets the selected file and adds it to the Arraylist for currently playing titles.
     * Then it opens the media window and closes this window.
     * @throws IOException
     */
    @FXML
    protected void playFileBtn() throws IOException {
        if(list.getSelectionModel().getSelectedItem() != null) {
            //gets path for mediafolder
            DB.selectSQL("select fldFilePath from tblMediaFolder where fldFolderID = '" + mediaFolder + "'");
            String path = DB.getDisplayData();
            path = path.replace(" ", "");
            path = path.replace("\n", "");

            //gets filename for selected file
            String file = list.getSelectionModel().getSelectedItem();
            file = file.replace(" ", "");
            file = file.replace("\n", "");

            //adds folder and file to a single path
            String fullPath = path + "/" + file;

            //creates file and adds it to the playlist arraylist (playerController())
            File play = new File(fullPath);
            PlayerController.playlist.clear();
            PlayerController.playlist.add(play);

            //closes this window and opens player window
            Main.playerWindow();
            Stage s = (Stage) playFile.getScene().getWindow();
            s.close();
        } else {
            //error message for not selecting a file
            Alert alert = new Alert(Alert.AlertType.ERROR, "Please select a file first.", ButtonType.OK);
            alert.showAndWait();
        }
    }


    /**
     * Method gets the selected playlist and adds all its files to the currently-playing Arraylist.
     * Then it opens the media player and closes this window.
     * @throws IOException
     */
    @FXML
    protected void playPlaylist() throws IOException {
        //check if playlist contains files
        if(list2.getItems().size() != 0) {

            //clear current playlist
            PlayerController.playlist.clear();

            //add all items from selected playlist to current playlist
            for (int i = 0; i < list2.getItems().size(); i++) {
                String itemFromList = list2.getItems().get(i);
                itemFromList = itemFromList.replace(" ", "");
                itemFromList = itemFromList.replace("\n", "");


                DB.selectSQL("select fldFilePath from tblMediaFolder where fldFolderID = '" + mediaFolder + "'");
                String path = DB.getDisplayData();
                path = path.replace(" ", "");
                path = path.replace("\n", "");

                String file = itemFromList;
                file = file.replace(" ", "");
                file = file.replace("\n", "");

                String fullPath = path + "/" + file;


                File play = new File(fullPath);
                PlayerController.playlist.add(play);
            }


            Main.playerWindow();
            Stage s = (Stage) playFile.getScene().getWindow();
            s.close();

        } else {
            //error message for not selecting a file
            Alert alert = new Alert(Alert.AlertType.ERROR, "You are trying to play an empty playlist.", ButtonType.OK);
            alert.showAndWait();
        }
    }


    /**
     * This reads file attributes from the database and sets the Labels to show them.
     */
    protected void setLabelFile(){
        String selected = list.getSelectionModel().getSelectedItem();

        if(selected != null) {
            //format text
            selected = selected.replace(" ", "");
            selected = selected.replace("\n", "");

            //select columns and set text
            DB.selectSQL("select fldArtist from tblFile where fldFileName = '" + selected + "'");
            do {
                String data = DB.getDisplayData();
                if (data.equals(DB.NOMOREDATA)) {
                    break;
                } else {
                    labelFile1.setText("Artist: " + data);
                }
            } while (true);

            DB.selectSQL("select fldTitle from tblFile where fldFileName = '" + selected + "'");
            do {
                String data = DB.getDisplayData();
                if (data.equals(DB.NOMOREDATA)) {
                    break;
                } else {
                    labelFile2.setText("Title: " + data);
                }
            } while (true);
        } else {
            labelFile1.setText("Artist: ");
            labelFile2.setText("Title: ");
        }
    }

    /**
     * This reads the description for a playlist from the database and sets the label to show them.
     */
    protected void setLabelPlaylist(){
        DB.selectSQL("select fldDescription from tblPlaylist where fldID = '"+ selectedPlaylistID +"'");
        do{
            String data = DB.getDisplayData();
            if (data.equals(DB.NOMOREDATA)){
                break;
            }else{
                labelPlaylist.setText(data);
            }
        } while(true);
    }







    /**
     * Method to look up the playlistID for a given title and saving it to selectedPlaylistID.
     * @param selectedPlaylist
     */
    protected void getIDforPlaylist(String selectedPlaylist){
        if(selectedPlaylist != null) {
            selectedPlaylist = selectedPlaylist.replace("\n", "");


            //get corresponding id to playlist title
            DB.selectSQL("select fldID from tblPlaylist where fldTitle = '" + selectedPlaylist + "'");
            String playlistIDLocal = DB.getDisplayData();

            playlistIDLocal = playlistIDLocal.replace("\n", "");

            selectedPlaylistID = Integer.parseInt(playlistIDLocal);
        }
    }

    /**
     * Method to clear list2(shows files in a playlist) and setting it to show new data.
     * like refresh method.
     */
    protected void setList2(){
        list2.getItems().clear();
        //get entries for selected playlist and display them
        DB.selectSQL("select fldFileName from [tblFile-Playlist] where fldPlaylistID = '" + selectedPlaylistID + "'");
        do{
            String data = DB.getDisplayData();
            if (data.equals(DB.NOMOREDATA)){
                break;
            }else{
                list2.getItems().add(data);
            }
        } while(true);
    }
}